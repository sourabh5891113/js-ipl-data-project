const fs = require("fs");
const convertCSVToJSON = require("../util");

//paths
let matchesCSVPath = "../data/matches.csv";
let deliveriesCSVPath = "../data/deliveries.csv";
let outputFilePath = "../public/output/extraRunsConcededPerTeam.json";

function extraRunsConcededPerTeam() {
  try {
    //getting all 2016 matches
    let matchesDataInJSON = convertCSVToJSON(matchesCSVPath);
    let matchesId2016 = [];
    for (let key of matchesDataInJSON) {
      if (key.season === "2016") {
        matchesId2016.push(key.id);
      }
    }

    //getting extra runs conceded per team
    let deliveriesDataInJSON = convertCSVToJSON(deliveriesCSVPath);
    let resultObj = {};
    for (let key of deliveriesDataInJSON) {
      if (matchesId2016.includes(key.match_id)) {
        let bowling_team = key.bowling_team;
        let extra_runs = Number(key.extra_runs);
        if (resultObj[bowling_team]) {
          resultObj[bowling_team] += extra_runs;
        } else {
          resultObj[bowling_team] = extra_runs;
        }
      }
    }
    console.log(resultObj);

    //Dumping output in output file.
    let data = JSON.stringify(resultObj);
    fs.writeFile(outputFilePath, data, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("Output Dumped successfully");
      }
    });
  } catch (err) {
    console.log(err);
  }
}

extraRunsConcededPerTeam();
