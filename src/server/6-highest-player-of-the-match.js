const fs = require("fs");
const convertCSVToJSON = require("../util");

//paths
let matchesCSVPath = "../data/matches.csv";
let outputFilePath = "../public/output/highestPlayerOfTheMatch.json";

function highestPlayerOfTheMatch() {
  try {
    //getting an object with season as key and value as an object of players as keys and value as number of times they have won POM
    let matchesDataInJSON = convertCSVToJSON(matchesCSVPath);
    let seasonsWithPOTM = {};
    for (let key of matchesDataInJSON) {
      let season = key.season;
      let player_of_match = key.player_of_match;
      if (seasonsWithPOTM[season]) {
        let POMobj = seasonsWithPOTM[season];
        if (POMobj[player_of_match]) {
          POMobj[player_of_match]++;
        } else {
          POMobj[player_of_match] = 1;
        }
      } else {
        seasonsWithPOTM[season] = { [player_of_match]: 1 };
      }
    }
    //getting an object with years as key and value as the highest number of times a player has won POM that year
    let seasonWithHIghestPOMObj = {};
    for (let key in seasonsWithPOTM) {
      let year = key;
      let POMobj = seasonsWithPOTM[key];
      let highestPOM = 0;
      for (let key in POMobj) {
        highestPOM = Math.max(highestPOM, POMobj[key]);
      }
      seasonWithHIghestPOMObj = {
        ...seasonWithHIghestPOMObj,
        [year]: highestPOM,
      };
    }
    //getting an object with key as seasons and value as array of players who have won POM hughest number of times
    let playersWithHighestPOM = {};
    for (let key in seasonsWithPOTM) {
      let year = key;
      let POMobj = seasonsWithPOTM[key];
      let highestPOMArr = [];
      for (let key in POMobj) {
        if (POMobj[key] === seasonWithHIghestPOMObj[year]) {
          highestPOMArr.push(key);
        }
      }
      playersWithHighestPOM = {
        ...playersWithHighestPOM,
        [year]: highestPOMArr,
      };
    }
    console.log(playersWithHighestPOM);

    //Dumping output in output file.
    let data = JSON.stringify(playersWithHighestPOM);
    fs.writeFile(outputFilePath, data, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("Output Dumped successfully");
      }
    });
  } catch (err) {
    console.log(err);
  }
}

highestPlayerOfTheMatch();
