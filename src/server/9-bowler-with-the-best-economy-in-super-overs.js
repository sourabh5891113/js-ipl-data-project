const fs = require("fs");
const convertCSVToJSON = require("../util");

//paths
let deliveriesCSVPath = "../data/deliveries.csv";
let outputFilePath =
  "../public/output/bowlerWithTheBestEconomyInSuperOvers.json";

function bowlerWithTheBestEconomyInSuperOvers() {
  try {
    //getting total balls bowled by every bowler and total runs conceded by him in super over
    let deliveriesDataInJSON = convertCSVToJSON(deliveriesCSVPath);
    let ballsObj = {};
    let totalRunObj = {};
    for (let key of deliveriesDataInJSON) {
      if (key.is_super_over !== "0") {
        let bowler = key.bowler;
        let total_runs = Number(key.total_runs);
        let IsWideOrNo =
          key.wide_runs !== "0" || key.noball_runs !== "0" ? true : false;
        if (ballsObj[bowler]) {
          if (!IsWideOrNo) {
            ballsObj[bowler]++;
          }
          totalRunObj[bowler] += total_runs;
        } else {
          ballsObj[bowler] = 1;
          totalRunObj[bowler] = total_runs;
        }
      }
    }
    //getting bowling average of every bowler in super over
    let resultObj = {};
    for (let key in ballsObj) {
      let avg = (totalRunObj[key] / ballsObj[key]) * 6;
      resultObj[key] = Math.round(avg * 100) / 100;
    }
    let keyValArr = [];
    for (let key in resultObj) {
      keyValArr.push([key, resultObj[key]]);
    }
    keyValArr.sort((a, b) => a[1] - b[1]);
    let res = "Most economical bowler in super over is " + keyValArr[0][0];

    console.log(res);

    //Dumping output in output file.
    let data = JSON.stringify(keyValArr[0][0]);
    fs.writeFile(outputFilePath, data, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("Output Dumped successfully");
      }
    });
  } catch (err) {
    console.log(err);
  }
}

bowlerWithTheBestEconomyInSuperOvers();
