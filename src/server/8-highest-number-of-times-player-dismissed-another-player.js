const fs = require("fs");
const convertCSVToJSON = require("../util");

//paths
let deliveriesCSVPath = "../data/deliveries.csv";
let outputFilePath =
  "../public/output/highestNumberOfTimesPlayerDismissedAnotherPlayer.json";

function highestNumberOfTimesPlayerDismissedAnotherPlayer() {
  try {
    //getting an object with key as player dismissed and value as an object of bowlers who dismissed him as key and number of times dismmised as value
    let deliveriesDataInJSON = convertCSVToJSON(deliveriesCSVPath);
    let playerDismissedAndBowler = {};
    for (let key of deliveriesDataInJSON) {
      let player_dismissed = key.player_dismissed;
      if (player_dismissed !== "") {
        let bowler = key.bowler;
        if (playerDismissedAndBowler[player_dismissed]) {
          let bowlerObj = playerDismissedAndBowler[player_dismissed];
          if (bowlerObj[bowler]) {
            bowlerObj[bowler]++;
          } else {
            bowlerObj[bowler] = 1;
          }
        } else {
          playerDismissedAndBowler[player_dismissed] = { [bowler]: 1 };
        }
      }
    }
    // console.log(playerDismissedAndBowler['DA Warner']);
    //getting an object with key as player dismissed and value as maximum number of times he got dismmised by a bowler
    let highestNOT = {};
    for (let key in playerDismissedAndBowler) {
      let player_dismissed = key;
      let bowlerObj = playerDismissedAndBowler[key];
      let max = 0;
      for (let bowler in bowlerObj) {
        max = Math.max(max, bowlerObj[bowler]);
      }
      highestNOT[player_dismissed] = max;
    }
    // console.log(highestNOT);

    //getting an object with player dismissed as key and an array of bowlers who dismissed him maximum number of times as value
    let highestNumberOfTimesPlayerDismissedAnotherPlayer = {};
    for (let key in playerDismissedAndBowler) {
      let player_dismissed = key;
      let bowlerObj = playerDismissedAndBowler[key];
      let maxNOT = highestNOT[key];
      let bowlerArr = [];
      for (let bowler in bowlerObj) {
        if (maxNOT === bowlerObj[bowler]) {
          bowlerArr.push(bowler);
        }
      }
      highestNumberOfTimesPlayerDismissedAnotherPlayer[player_dismissed] =
        bowlerArr;
    }
    // console.log(highestNumberOfTimesPlayerDismissedAnotherPlayer);

    // //Dumping output in output file.
    let data = JSON.stringify(highestNumberOfTimesPlayerDismissedAnotherPlayer);
    fs.writeFile(outputFilePath, data, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("Output Dumped successfully");
      }
    });
  } catch (err) {
    console.log(err);
  }
}

highestNumberOfTimesPlayerDismissedAnotherPlayer();
