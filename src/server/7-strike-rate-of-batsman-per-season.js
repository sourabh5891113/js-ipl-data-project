const fs = require("fs");
const convertCSVToJSON = require("../util");

//paths
let matchesCSVPath = "../data/matches.csv";
let deliveriesCSVPath = "../data/deliveries.csv";
let outputFilePath = "../public/output/strikeRateOfBatsmanPerSeason.json";

function strikeRateOfBatsmanPerSeason(batsman) {
  try {
    let SROfAllBatsman = strikeRateOfAllBatsmansPerSeason();
    let result = {};
    for (let key in SROfAllBatsman) {
      let season = key;
      let playersObj = SROfAllBatsman[key];
      for (let player in playersObj) {
        if (player === batsman) {
          result[season] = playersObj[player];
        }
      }
    }
    console.log(batsman + "'s Strike rate in every season");
    console.log(result);

    // //Dumping output in output file.
    let data = JSON.stringify(result);
    fs.writeFile(outputFilePath, data, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("Output Dumped successfully");
      }
    });
  } catch (err) {
    console.log(err);
  }
}
strikeRateOfBatsmanPerSeason("DA Warner");

function strikeRateOfAllBatsmansPerSeason() {
  try {
    //getting an object in which key is season and value is an array cnatining ids of all matches happing in that season
    let matchesDataInJSON = convertCSVToJSON(matchesCSVPath);
    let matchIDYearWise = {};
    for (let key of matchesDataInJSON) {
      let year = key.season;
      if (matchIDYearWise[year]) {
        let matchIDArr = matchIDYearWise[year];
        matchIDArr = [...matchIDArr, key.id];
        matchIDYearWise[year] = matchIDArr;
      } else {
        matchIDYearWise[year] = [key.id];
      }
    }
    //   console.log(matchIDYearWise);

    let strikeRateOfBatsmanPerSeason = {};
    for (let key in matchIDYearWise) {
      let season = key;
      let matchIDArr = matchIDYearWise[season];

      //getting total balls played and total runs scored by a batsman in a perticular season
      let deliveriesDataInJSON = convertCSVToJSON(deliveriesCSVPath);
      let playerWithTotalBalls = {};
      let playerWithTotalRuns = {};
      for (let key of deliveriesDataInJSON) {
        if (matchIDArr.includes(key.match_id)) {
          let batsman = key.batsman;
          let batsman_runs = Number(key.batsman_runs);
          let IsWideOrNo =
            key.wide_runs !== "0" || key.noball_runs !== "0" ? true : false;
          if (!IsWideOrNo) {
            if (playerWithTotalBalls[batsman]) {
              playerWithTotalBalls[batsman]++;
            } else {
              playerWithTotalBalls[batsman] = 1;
            }
          }
          if (playerWithTotalRuns[batsman]) {
            playerWithTotalRuns[batsman] += batsman_runs;
          } else {
            playerWithTotalRuns[batsman] = batsman_runs;
          }
        }
      }
      //getting strike rate of a batsman for given season
      let strikeRateOfBatsman = {};
      for (let key in playerWithTotalBalls) {
        let batman = key;
        let batsman_runs = playerWithTotalRuns[key];
        let total_balls = playerWithTotalBalls[key];
        let strikeRate = (batsman_runs / total_balls) * 100;
        strikeRateOfBatsman = { ...strikeRateOfBatsman, [batman]: strikeRate };
      }
      strikeRateOfBatsmanPerSeason[season] = strikeRateOfBatsman;
    }

    return strikeRateOfBatsmanPerSeason;
  } catch (err) {
    console.log(err);
  }
}
