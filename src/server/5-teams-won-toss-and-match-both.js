const fs = require("fs");
const convertCSVToJSON = require("../util");

//paths
let matchesCSVPath = "../data/matches.csv";
let outputFilePath = "../public/output/teamsWonTossAndMatchBoth.json";

function teamsWonTossAndMatchBoth() {
  try {
    let matchesDataInJSON = convertCSVToJSON(matchesCSVPath);
    let resultObj = {};
    for (let key of matchesDataInJSON) {
      let toss_winner = key.toss_winner;
      let winner = key.winner;
      if (toss_winner === winner) {
        if (resultObj[winner]) {
          resultObj[winner]++;
        } else {
          resultObj[winner] = 1;
        }
      }
    }
    console.log(resultObj);

    //Dumping output in output file.
    let data = JSON.stringify(resultObj);
    fs.writeFile(outputFilePath, data, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("Output Dumped successfully");
      }
    });
  } catch (err) {
    console.log(err);
  }
}

teamsWonTossAndMatchBoth();
