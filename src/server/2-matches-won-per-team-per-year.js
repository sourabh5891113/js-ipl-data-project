const fs = require("fs");
const convertCSVToJSON = require("../util");

//paths
let matchesCSVPath = "../data/matches.csv";
let outputFilePath = "../public/output/matchesWonPerTeamPerYear.json";

function matchesWonPerTeamPerYear() {
  try {
    let matchesDataInJSON = convertCSVToJSON(matchesCSVPath);
    let resultObj = {};
    for (let key of matchesDataInJSON) {
      let winner = key.winner;
      let season = key.season;
      if (resultObj[winner]) {
        let winnerObj = resultObj[winner];
        if (winnerObj[season]) {
          winnerObj[season]++;
        } else {
          winnerObj[season] = 1;
        }
      } else {
        resultObj[winner] = { [season]: 1 };
      }
    }
    console.log(resultObj);

    //Dumping output in output file.
    let data = JSON.stringify(resultObj);
    fs.writeFile(outputFilePath, data, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("Output Dumped successfully");
      }
    });
  } catch (err) {
    console.log(err);
  }
}

matchesWonPerTeamPerYear();
