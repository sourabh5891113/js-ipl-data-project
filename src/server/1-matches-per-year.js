const fs = require("fs");
const convertCSVToJSON = require("../util");

//paths
let matchesCSVPath = "../data/matches.csv";
let outputFilePath = "../public/output/matchesPerYear.json";

//function for matches per year
function matchesPerYear() {
  try {
    let matchesDataInJSON = convertCSVToJSON(matchesCSVPath);
    let resultObj = {};
    for (let key of matchesDataInJSON) {
      let matchYear = key.season;
      if (resultObj[matchYear]) {
        resultObj[matchYear]++;
      } else {
        resultObj[matchYear] = 1;
      }
    }
    console.log(resultObj);

    //Dumping output in output file.
    let data = JSON.stringify(resultObj);
    fs.writeFile(outputFilePath, data, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("Output Dumped successfully");
      }
    });
  } catch (err) {
    console.log(err);
  }
}

matchesPerYear();
