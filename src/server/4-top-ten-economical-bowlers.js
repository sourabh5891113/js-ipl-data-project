const fs = require("fs");
const convertCSVToJSON = require("../util");

//paths
let matchesCSVPath = "../data/matches.csv";
let deliveriesCSVPath = "../data/deliveries.csv";
let outputFilePath = "../public/output/topTenEconomicalBowlers.json";

function topTenEconomicalBowlers() {
  try {
    //getting all 2015 matches
    let matchesDataInJSON = convertCSVToJSON(matchesCSVPath);
    let matchesId2015 = [];
    for (let key of matchesDataInJSON) {
      if (key.season === "2015") {
        matchesId2015.push(key.id);
      }
    }

    //getting total balls bowled by every bowler and total runs conceded by him
    let deliveriesDataInJSON = convertCSVToJSON(deliveriesCSVPath);
    let ballsObj = {};
    let totalRunObj = {};
    for (let key of deliveriesDataInJSON) {
      if (matchesId2015.includes(key.match_id)) {
        let bowler = key.bowler;
        let total_runs = Number(key.total_runs);
        let IsWideOrNo =
          key.wide_runs !== "0" || key.noball_runs !== "0" ? true : false;
        if (ballsObj[bowler]) {
          if (!IsWideOrNo) {
            ballsObj[bowler]++;
          }
          totalRunObj[bowler] += total_runs;
        } else {
          ballsObj[bowler] = 1;
          totalRunObj[bowler] = total_runs;
        }
      }
    }

    //getting bowling average of every bowler
    let resultObj = {};
    for (let key in ballsObj) {
      let avg = (totalRunObj[key] / ballsObj[key]) * 6;
      resultObj[key] = Math.round(avg * 100) / 100;
    }
    let keyValArr = [];
    for (let key in resultObj) {
      keyValArr.push([key, resultObj[key]]);
    }
    keyValArr.sort((a, b) => a[1] - b[1]);

    //getting top ten economical bowlers
    let topTenEconomicalBowlersObj = {};
    for (let index = 0; index < 10; index++) {
      topTenEconomicalBowlersObj = {
        ...topTenEconomicalBowlersObj,
        [index + 1]: keyValArr[index][0],
      };
    }
    console.log(topTenEconomicalBowlersObj);

    //Dumping output in output file.
    let data = JSON.stringify(topTenEconomicalBowlersObj);
    fs.writeFile(outputFilePath, data, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("Output Dumped successfully");
      }
    });
  } catch (err) {
    console.log(err);
  }
}

topTenEconomicalBowlers();
